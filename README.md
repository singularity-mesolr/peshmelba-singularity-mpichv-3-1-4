# singularity-peshmelba-mpich-3.1.4
* Singularity container with PALM installed (insert link to PALM)
* Conda2 is also installed in order to get a gcc 7.5 environment
* Spécific Python libraries are listed here =>
```bash
    pip install cycler==0.10.0
    pip install mpi4py==3.0.3
    pip install Cython==0.26.1
    pip install numpy==1.11.0
    pip install pyparsing==2.4.6
    pip install python-dateutil==2.8.1
    pip install pytz==2019.3
    pip install six==1.14.0
    pip install scipy==1.2.2
```

## install singularity 3.6.3 , see 
## https://forgemia.inra.fr/pepi-atelier-singularity/atelier-singularity/


## BUILD - careful you must build it locally, this is not psssible in the cluster! (so install singularity locally too)
```bash
git clone https://forgemia.inra.fr/singularity-mesolr/peshmelba-singularity-mpichc-3-1-4.git
singularity build peshmelba-mesolr/peshmelba-singularity-mpichv-3-1-4.sif peshmelba-mesolr/peshmelba-singularity-mpichv-3-1-4.def
```

## USAGE

First, pull the image and download the "slurm-peshmelba-singularity.sh" file =>
```bash
singularity pull peshmelba-singularity-mpichv-3-1-4.sif oras://registry.forgemia.inra.fr/singularity-mesolr/peshmelba-singularity-mpichv-3-1-4/peshmelba-singularity-mpichv-3-1-4:latest
```

Modify those variable in slurm-peshmelba-singularity.sh according to your work directory and image (sif) path =>
```bash
# Path to singularity image of peshmelba
SIF_PATH=/lustre/verrierj/NEW-PESHMELBA/peshmelba-singularity-mpichv-3-1-4.sif

# Main working FOLDER
MAIN_PATH=/lustre/verrierj/NEW-PESHMELBA
```

Then create all folders OR add to slurm-peshmelba-singularity.sh "mkdir FOLDERS"

The app needs 3 folders =>
```bash
PESHMELBA
OUTPUT
INPUT
```

* INPUT needs to have INPUT DATA for PESHMELBA
* OUTPUT is where all results will be stored
* PESHMELBA is for PESHMELBA temp file

All those folders are then mounted (via bind) in the singularity container.

Allowing you to make has many simulations as needed of PESHMELBA by giving the script multiple INPUT folders.

## To launch on Meso@LR with SLURM =>

```bash
sbatch slurm-peshmelba-singularity.sh
```
