
#! /usr/bin/env bash

#SBATCH -N 1
#SBATCH -A inrae-infra

# 14 processus mpi sont lancés par le mpiexec

#SBATCH -c 14

#Les bons modules =>

module load cv-standard
module load singularity/3.6.3
module load mvapich2/gcc61/2.2rc2

# Chemin vers l'image singularity
SIF_PATH=/lustre/verrierj/NEW-PESHMELBA/peshmelba-singularity-mpichv-3-1-4.sif

# Le dossier à monter pour calculer
MAIN_PATH=/lustre/verrierj/NEW-PESHMELBA
INPUT_PATH=${MAIN_PATH}/INPUT
OUTPUT_PATH=${MAIN_PATH}/OUTPUT
PESHMELBA_PATH=${MAIN_PATH}/PESHMELBA

# Export pour point de montage
export SINGULARITY_BINDPATH="${INPUT_PATH}:/opt/INPUT,${OUTPUT_PATH}:/opt/OUTPUT,${PESHMELBA_PATH}:/opt/PESHMELBA"

# On lance la compilation de PESHMELBA STEP 2 et 3
singularity exec "${SIF_PATH}" bash peshmelba-step-2-3.sh

# La commande singularity à lancer
SING_PESH="singularity run"


# Il faut isoler chaque tâche MPI ce qui fait une ligne très longue mais qui marche !
mpiexec -np 1 ${SING_PESH} ${SIF_PATH}  ./palm_main : -np 1 ${SING_PESH} ${SIF_PATH}  ./main_block_1 : -np 1 ${SING_PESH} ${SIF_PATH}  python ./main_init_CONF.py : -np 1 ${SING_PESH} ${SIF_PATH}  ./main_INIT_FRER1D : -np 1 ${SING_PESH} ${SIF_PATH}  python ./main_EXCHANGES_SURFACE.py : -np 1 ${SING_PESH} ${SIF_PATH}  python ./main_RIVER1D.py : -np 1 ${SING_PESH} ${SIF_PATH}  ./main_FRER1D_bloc : -np 1 ${SING_PESH} ${SIF_PATH}  ./main_SYNCHRO_init_2 : -np 1 ${SING_PESH} ${SIF_PATH}  python ./main_writer_PY.py : -np 1 ${SING_PESH} ${SIF_PATH}  python ./main_reader_DA.py : -np 1 ${SING_PESH} ${SIF_PATH}  python ./main_writer_DA.py : -np 1 ${SING_PESH} ${SIF_PATH}  python ./main_init_AS.py : -np 1 ${SING_PESH} ${SIF_PATH}  python ./main_test_python.py : -np 1 ${SING_PESH} ${SIF_PATH}  python ./main_EXCHANGES_SUBSURFACE.py

rm -fr ${PESHMELBA_PATH}

